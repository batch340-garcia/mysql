-- Add New Records
--  Add 5 5 artists, 2 albums each, 2 songs per album
-- artists
INSERT INTO artists	(name) VALUES ("Taylor Swift");
INSERT INTO artists	(name) VALUES ("Lady Gaga");
INSERT INTO artists	(name) VALUES ("Justine Bieber");
INSERT INTO artists	(name) VALUES ("Arianda Grande");
INSERT INTO artists	(name) VALUES ("Bruno Mars");

-- Taylor Swift
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Fearless", "2008-1-1", 6);

INSERT INTO songs (songs_name, length, genre, album_id) VALUES ("Fearless", 246, "Pop Rock", 6);

INSERT INTO songs (songs_name, length, genre, album_id) VALUES ("Love Story", 213, "Country Pop", 6);

-- 

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Red", "2012-1-1", 3);

INSERT INTO songs (songs_name, length, genre, album_id) VALUES ("State of Grace", 243, "Rock, Alternative Rock", 7);

INSERT INTO songs (songs_name, length, genre, album_id) VALUES ("Red", 204, "Country", 7);


-- Lady Gaga

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("A Star is Born", "2018-1-1", 7);

INSERT INTO songs (songs_name, length, genre, album_id) VALUES ("Black Eyes", 141, "Rock and Roll", 8);

INSERT INTO songs (songs_name, length, genre, album_id) VALUES ("Shallow", 201, "Country, Rock, Folk Rock", 8);

-- 

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Born This Way", "2011-1-1", 7);

INSERT INTO songs (songs_name, length, genre, album_id) VALUES ("Born This Way", 252, "Electropop", 9);

INSERT INTO songs (songs_name, length, genre, album_id) VALUES ("Marry The Nigth", 201, "Electropop", 9);


-- Justin Bieber
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Purpose", "2012-1-1", 8);

INSERT INTO songs (songs_name, length, genre, album_id) VALUES ("Sorry", 152, "Dancehall-poptropical housemoombahton", 10);


INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Believe", "2012-1-1", 8);

INSERT INTO songs (songs_name, length, genre, album_id) VALUES ("Boyfriend", 251, "Pop", 11);


-- Arianda Grande

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Dangerous Women", "2016-1-1", 9);

INSERT INTO songs (songs_name, length, genre, album_id) VALUES ("Into You", 251, "EDM House", 12);


INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Thank U, Next", "2019-1-1", 9);

INSERT INTO songs (songs_name, length, genre, album_id) VALUES ("Thank U, Next", 146, "Pop, R&B", 13);

-- Bruno Mars

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("24K Magic", "2016-1-1", 10);

INSERT INTO songs (songs_name, length, genre, album_id) VALUES ("24K Magic", 204, "Funk, Disco, R&B", 14);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Earth to Mars", "2011-1-1", 10);

INSERT INTO songs (songs_name, length, genre, album_id) VALUES ("Lost", 132, "Pop", 15);



 -- [SECTION] Advanced Selects

 -- Exclude Records
 SELECT * FROM songs WHERE id != 11;

 -- 
 SELECT * FROM songs WHERE genre != "OPM";

 -- 
 SELECT * FROM songs WHERE album_id != 5;

 -- Greater than or equal
 SELECT * FROM songs WHERE id < 11;
  SELECT * FROM songs WHERE id > 11;


  -- 
  SELECT * FROM songs WHERE length > 250;

  -- Get specific songs throgh ID using OR
  SELECT * FROM songs WHERE id = 1 OR id = 3 OR id = 5;

  -- IN includes the value in WHERE that we provided.
  SELECT * FROM songs WHERE id IN (1, 3, 5);

  SELECT * FROM songs WHERE genre IN ("Pop", "K-Pop");

  -- Combine Conditions
  SELECT * FROM songs WHERE album_id = 4 AND id < 8;

  -- 
  SELECT * FROM songs WHERE songs_name LIKE "%a"; -- select keyword from the end
  SELECT * FROM songs WHERE songs_name LIKE "a%"; -- select keyword from the start
  SELECT * FROM songs WHERE songs_name LIKE "%a%"; -- select keyword from the in-between.

  SELECT * FROM albums WHERE date_released LIKE "201_%"; -- select keyword from the start
    SELECT * FROM albums WHERE date_released LIKE "201______"; -- if this is the _ should be exact lenght of the remaining char.
  SELECT * FROM albums WHERE album_title LIKE "Pur_ose"; -- know specific pattern


-- Sorting records

SELECT * FROM songs ORDER BY songs_name DESC; -- descending order

SELECT * FROM songs ORDER BY songs_name ASC; -- ascending order

-- Getting distinct records
SELECT DISTINCT genre FROM songs;

-- Case sensitive query
SELECT * FROM songs WHERE genre = BINARY "pop";

SELECT * FROM songs WHERE songs_name LIKE BINARY "B%";


-- [SECTION] joining Tables

-- Combine artist and albums table
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id;

-- Combine more than two tables
SELECT * FROM artists 
	JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;

-- Select columns to be inclded per table
SELECT artists.name, albums.album_title FROM artists 
	JOIN albums ON artists.id = albums.artist_id;