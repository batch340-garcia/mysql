-- [SECTION] Inserting Records - CREATE

-- artist
INSERT INTO artists (name) VALUES ("Rivermaya");


-- albums
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Psy 6", "2012-1-1", 2);

-- songs
INSERT INTO songs (songs_name, length, genre, album_id) VALUES ("Gangnam Style", 253, "K-Pop", 4);

INSERT INTO songs (songs_name, length, genre, album_id) VALUES ("Kisapmata", 279, "OPM", 2);


-- [SELECTION] Selecting Records 0- READ

-- Selecting the songs_name and genre of all songs
SELECT songs_name, genre FROM songs;

-- Selecting all of the songs
SELECT * FROM songs;

-- Selecting all songs_name of all OPM genre sogs
SELECT songs_name FROM songs WHERE genre = "OPM";

-- 
SELECT album_title, artist_id FROM albums;


-- We can use AND and OR for multipleexpressions in the WHERE clause
SELECT songs_name, length FROM songs WHERE length > 240 AND genre = "K-Pop";
SELECT songs_name, length FROM songs WHERE length > 240 OR genre = "OPM";

-- [SECTION] Updating Records - UPDATE

-- Update the lenght of Gangnam Style to 240
UPDATE songs SET length = 240 WHERE songs_name = "Gangnam Style";

-- Note: Removing the WHERE clause will update all rows

UPDATE songs SET length = 253 WHERE songs_name = "Kisapmata";

-- [SECTION] Deleting Records - DELETE
DELETE FROM songs WHERE genre = "OPM" AND length > 240;

-- Note: Removing the WHERE clause will delete all rows


-- hello fred